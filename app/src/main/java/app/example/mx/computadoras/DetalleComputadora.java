package app.example.mx.computadoras;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class DetalleComputadora extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private TextView nombreTextView;
    private TextView marcaTextView;
    private TextView modeloTextView;
    private TextView serieTextView;
    private TextView caracteristicasTextView;
    private TextView precioTextView;


    private long computadoraId;
    public static final String EXTRA_computadora_ID = "computadora.id.extra";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_computadora);

        nombreTextView = (TextView) findViewById(R.id.nombre_text_view);
        marcaTextView = (TextView) findViewById(R.id.marca_text_view);
        modeloTextView = (TextView) findViewById(R.id.modelo_text_view);
        serieTextView = (TextView) findViewById(R.id.serie_text_view);
        caracteristicasTextView = (TextView) findViewById(R.id.caracteristicas_text_view);
        precioTextView = (TextView) findViewById(R.id.precio_text_view);

        Intent intencion = getIntent();
        computadoraId = intencion.getLongExtra(MainActivity.EXTRA_ID_computadora, -1L);

        getSupportLoaderManager().initLoader(0, null, this);

        findViewById(R.id.boton_eliminar).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        new DeletecomputadoraTask(DetalleComputadora.this,computadoraId).execute();

                    }
                }
        );

        findViewById(R.id.boton_actualizar).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent =new Intent(DetalleComputadora.this, AgregarComputadora.class);
                        intent.putExtra(EXTRA_computadora_ID, computadoraId);
                        startActivity(intent);


                    }
                }
        );


    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new ComputadoraLoader(this,computadoraId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if(data != null && data.moveToFirst()){

            int nameIndex = data.getColumnIndexOrThrow(ComputadorasDatabase.COL_NAME);
            String nombrecomputadora= data.getString(nameIndex);

            int marcaIndex = data.getColumnIndexOrThrow(ComputadorasDatabase.COL_MARCA);
            String marcacomputadora= data.getString(marcaIndex);

            int modeloIndex = data.getColumnIndexOrThrow(ComputadorasDatabase.COL_MODELO);
            String modelocomputadora= data.getString(modeloIndex);

            int serieIndex = data.getColumnIndexOrThrow(ComputadorasDatabase.COL_SERIE);
            String seriecomputadora= data.getString(serieIndex);

            int caracteristicasIndex = data.getColumnIndexOrThrow(ComputadorasDatabase.COL_CARACTERISTICAS);
            String caracreristicascomputadora= data.getString(caracteristicasIndex);

            int precioIndex = data.getColumnIndexOrThrow(ComputadorasDatabase.COL_PRECIO);
            String preciocomputadora= data.getString(precioIndex);


            nombreTextView.setText(nombrecomputadora);
            marcaTextView.setText(marcacomputadora);
            modeloTextView.setText(modelocomputadora);
            serieTextView.setText(seriecomputadora);
            caracteristicasTextView.setText(caracreristicascomputadora);
            precioTextView.setText(preciocomputadora);

        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }


    public static class DeletecomputadoraTask extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<Activity> weakActivity;
        private long computadoraId;

        public DeletecomputadoraTask(Activity activity, long id){
            weakActivity = new WeakReference<Activity>(activity);
            computadoraId = id;

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Context context = weakActivity.get();
            if (context == null){
                return false;
            }

            Context appContext = context.getApplicationContext();

            int filasAfectadas = ComputadorasDatabase.eliminaConId(appContext, computadoraId);
            return  (filasAfectadas != 0);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Activity context = weakActivity.get();
            if(context == null){
                return;
            }
            if (aBoolean){
                Toast.makeText(context, "Se elimino correctamente", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, "No se pudo guardar", Toast.LENGTH_SHORT).show();
            }
            context.finish();
        }
    }
}