package app.example.mx.computadoras;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.support.v4.content.LocalBroadcastManager;



public class ComputadorasDatabase extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "computadoras.db";
    private static final String TABLE_NAME = "computadoras";
    public static final String COL_NAME = "nombre";
    public static final String COL_MARCA = "marca";
    public static final String COL_MODELO = "modelo";
    public static final String COL_SERIE = "serie";
    public static final String COL_CARACTERISTICAS = "caracteristicas";
    public static final String COL_PRECIO = "precio";


    public ComputadorasDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String createQuery =
                "CREATE TABLE " + TABLE_NAME +
                        " (_id INTEGER PRIMARY KEY, "
                        + COL_NAME + " TEXT NOT NULL COLLATE UNICODE, "
                        + COL_MARCA + " TEXT NOT NULL, "
                        + COL_MODELO + " TEXT NOT NULL, "
                        + COL_SERIE + " TEXT NOT NULL, "
                        + COL_CARACTERISTICAS + " TEXT NOT NULL, "
                        + COL_PRECIO + " TEXT NOT NULL)";

    db.execSQL(createQuery);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String upgradeQuery = "DROP TABLE IF EXIST " + TABLE_NAME;
        db.execSQL(upgradeQuery);

    }

    public static long insertaComputadora (Context context, String nombre,  String marca, String modelo, String serie, String caracteristicas, String precio ){

        SQLiteOpenHelper dbOpenHelper = new ComputadorasDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        ContentValues valorcomputadora = new ContentValues();
        valorcomputadora.put(COL_NAME,nombre);
        valorcomputadora.put(COL_MARCA,marca);
        valorcomputadora.put(COL_MODELO,modelo);
        valorcomputadora.put(COL_SERIE,serie);
        valorcomputadora.put(COL_CARACTERISTICAS,caracteristicas);
        valorcomputadora.put(COL_PRECIO,precio);

        long result = -1L;
        try {
            result = database.insert(TABLE_NAME, null, valorcomputadora);

            if (result != -1L)
                {
                    LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
                    Intent intentFilter = new Intent(ComputadorasLoader.ACTION_RELOAD_TABLE);
                    broadcastManager.sendBroadcast(intentFilter);
                }
            } finally  {
                dbOpenHelper.close();
            }


        return result;

    }

    public static Cursor devuelveTodos(Context context){

        SQLiteOpenHelper dbOpenHelper = new ComputadorasDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getReadableDatabase();

        return database.query(
                TABLE_NAME,
                new String[]{COL_NAME, COL_MARCA, COL_MODELO, COL_SERIE, COL_CARACTERISTICAS, COL_PRECIO, BaseColumns._ID},
                null, null, null, null,
                COL_NAME+ " ASC");

    }

    public static Cursor devuelveConId(Context context, long identificador){

        SQLiteOpenHelper dbOpenHelper = new ComputadorasDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getReadableDatabase();

        return database.query(
                TABLE_NAME,
                new String[]{COL_NAME, COL_MARCA, COL_MODELO, COL_SERIE, COL_CARACTERISTICAS, COL_PRECIO, BaseColumns._ID},
                BaseColumns._ID + " = ?",
                new String[]{String.valueOf(identificador)},
                null, null,
                COL_NAME+ " ASC");
    }


    public static int eliminaConId(Context context, long computadoraId) {

        SQLiteOpenHelper dbOpenHelper = new ComputadorasDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        int resultado = database.delete(
                TABLE_NAME, BaseColumns._ID + "=?",
                new String[] {String.valueOf(computadoraId)});

        if (resultado != 0)
        {
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
            Intent intentFilter = new Intent(ComputadorasLoader.ACTION_RELOAD_TABLE);
            broadcastManager.sendBroadcast(intentFilter);
        }

        dbOpenHelper.close();
        return resultado;
    }


    public static int actualizaComputadora (Context context, String nombre,  String marca, String modelo, String serie, String caracteristicas, String precio, long computadoraId){

        SQLiteOpenHelper dbOpenHelper = new ComputadorasDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        ContentValues valorcomputadora = new ContentValues();
        valorcomputadora.put(COL_NAME,nombre);
        valorcomputadora.put(COL_MARCA,marca);
        valorcomputadora.put(COL_MODELO,modelo);
        valorcomputadora.put(COL_SERIE,serie);
        valorcomputadora.put(COL_CARACTERISTICAS,caracteristicas);
        valorcomputadora.put(COL_PRECIO,precio);


        int result = database.update(
                TABLE_NAME,
                valorcomputadora,
                BaseColumns._ID + " =?",
                new String[]{String.valueOf(computadoraId)});

            if (result != 0){

                LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
                Intent intentFilter = new Intent(ComputadorasLoader.ACTION_RELOAD_TABLE);
                broadcastManager.sendBroadcast(intentFilter);
            }

            dbOpenHelper.close();

        return result;

}
}
