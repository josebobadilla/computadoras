package app.example.mx.computadoras;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class AgregarComputadora extends AppCompatActivity implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    private long computadoraId;
    private EditText computadoraEditText;
    private EditText marcaEditText;
    private EditText modeloEditText;
    private EditText serieEditText;
    private EditText caracteristicasEditText;
    private EditText precioEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_computadora);

        computadoraEditText = (EditText) findViewById(R.id.nombre_edit_text);
        marcaEditText = (EditText) findViewById(R.id.marca_edit_text);
        modeloEditText = (EditText) findViewById(R.id.modelo_edit_text);
        serieEditText = (EditText) findViewById(R.id.serie_edit_text);
        caracteristicasEditText = (EditText) findViewById(R.id.caracteristicas_edit_text);
        precioEditText = (EditText) findViewById(R.id.precio_edit_text);

        computadoraId = getIntent().getLongExtra(DetalleComputadora.EXTRA_computadora_ID, -1L);

        if (computadoraId != -1L){
            getSupportLoaderManager().initLoader(0, null, this);
        }

        findViewById(R.id.boton_agregar).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        String nombreComputadora = computadoraEditText.getText().toString();
        String marcaComputadora = marcaEditText.getText().toString();
        String modeloComputadora = modeloEditText.getText().toString();
        String serieComputadora = serieEditText.getText().toString();
        String caracteristicasComputadora = caracteristicasEditText.getText().toString();
        String precioComputadora = precioEditText.getText().toString();

        new CreateComputadoraTask(this, nombreComputadora, marcaComputadora, modeloComputadora, serieComputadora, caracteristicasComputadora, precioComputadora, computadoraId).execute();

    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new ComputadoraLoader(this,computadoraId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if(data != null && data.moveToFirst()){

            int nameIndex = data.getColumnIndexOrThrow(ComputadorasDatabase.COL_NAME);
            String nombreComputadora= data.getString(nameIndex);

            int marcaIndex = data.getColumnIndexOrThrow(ComputadorasDatabase.COL_MARCA);
            String marcaComputadora= data.getString(marcaIndex);

            int modeloIndex = data.getColumnIndexOrThrow(ComputadorasDatabase.COL_MODELO);
            String modeloComputadora= data.getString(modeloIndex);

            int serieIndex = data.getColumnIndexOrThrow(ComputadorasDatabase.COL_SERIE);
            String serieComputadora= data.getString(serieIndex);

            int caracteristicasIndex = data.getColumnIndexOrThrow(ComputadorasDatabase.COL_CARACTERISTICAS);
            String caracteristicasComputadora= data.getString(caracteristicasIndex);

            int precioIndex = data.getColumnIndexOrThrow(ComputadorasDatabase.COL_PRECIO);
            String precioComputadora= data.getString(precioIndex);

            computadoraEditText.setText(nombreComputadora);
            marcaEditText.setText(marcaComputadora);
            modeloEditText.setText(modeloComputadora);
            serieEditText.setText(serieComputadora);
            caracteristicasEditText.setText(caracteristicasComputadora);
            precioEditText.setText(precioComputadora);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public static class CreateComputadoraTask extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<Activity> weakActivity;
        private String computadoraName;
        private String computadoraMarc;
        private String computadoraMode;
        private String computadoraSeri;
        private String computadoraCara;
        private String computadoraPrec;


        private long computadoraId;

        public CreateComputadoraTask(Activity activity, String name, String marc, String mode, String seri, String cara, String prec, long computadoraId){
            weakActivity = new WeakReference<Activity>(activity);
            computadoraName = name;
            computadoraMarc = marc;
            computadoraMode = mode;
            computadoraSeri = seri;
            computadoraCara = cara;
            computadoraPrec = prec;

            this.computadoraId = computadoraId;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Context context = weakActivity.get();
            if (context == null){
                return false;
            }

            Context appContext = context.getApplicationContext();

            Boolean success = false;

            if (computadoraId != -1L) {
                int filasAfectadas = ComputadorasDatabase.actualizaComputadora(appContext, computadoraName, computadoraMarc, computadoraMode, computadoraSeri, computadoraCara, computadoraPrec, computadoraId);
                success = (filasAfectadas !=0);
            } else {
                long id = ComputadorasDatabase.insertaComputadora(appContext, computadoraName, computadoraMarc, computadoraMode, computadoraSeri, computadoraCara, computadoraPrec);
                success = (id != -1L);
            }
            return success;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Activity context = weakActivity.get();
            if(context == null){
                return;
            }
            if (aBoolean){
                Toast.makeText(context, "Se guardo correctamente", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, "No se pudo guardar", Toast.LENGTH_SHORT).show();
            }
            context.finish();
        }
    }

}